var Demo = (function () {

    
    function demoUpload() {
        var $uploadCrop;
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    });
                    $('.upload-demo').addClass('ready');
                    // $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
            else {
                alert("Sorry - you're browser doesn't support the FileReader API");
            }
        }
        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 250,
                height: 250,
                type: 'circle'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });
        $('#upload').on('change', function () {
            readFile(this);
        });
        $('.upload-result').on('click', function (ev) {
            ev.preventDefault();
            $uploadCrop.croppie('result', 'canvas').then(function (resp) {
                document.getElementById('img').value = resp;
            });
        });
    }
    function init() {
        demoUpload();
    }
    return {
        init: init
    };
})();
// Full version of `log` that:
//  * Prevents errors on console methods when no console present.
//  * Exposes a global 'log' function that preserves line numbering and formatting.
(function () {
    var method;
    var noop = function () {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});
    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
    if (Function.prototype.bind) {
        window.log = Function.prototype.bind.call(console.log, console);
    }
    else {
        window.log = function () {
            Function.prototype.apply.call(console.log, console, arguments);
        };
    }
})();
