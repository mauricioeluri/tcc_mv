<?php

function conecta() {
    $ret = pg_connect("host=localhost port=5432 dbname=nucleo_anim user=postgres password=postgres")
            or die('Erro conexao com o banco' . pg_last_erro());
    return $ret;
}

//Validar preenchimento obrigat�rio 
function obrigatorio($campo, $mostrar) {
    global $erro;
    if (trim($campo) == "") {
        $erro .= "<li id='erro'>Preenchimento Obrigatório: $mostrar</li> \n";
        return false;
    }

}

function ageCalculator($dob) {
    if (!empty($dob)) {
        list($year, $month, $day) = explode("-", $dob);
        $year_diff = date("Y") - $year;
        $month_diff = date("m") - $month;
        $day_diff = date("d") - $day;
        if ($day_diff < 0 || $month_diff < 0) {
            $year_diff--;
        }
        return $year_diff;
    } else {
        return '0';
    }
}


$teste = "agrvai";
