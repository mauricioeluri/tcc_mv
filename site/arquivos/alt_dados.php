<?php

//if (!isset($_POST)) {
//    echo 'nao tem post';
//    //header('Location: http://www.google.com/');
// }
//        if (!isset($_REQUEST['id_usu'])) {
//            header('Location: ../usuario/');
//        }
include 'estrutura/header.php';
if (!isset($_SESSION['usuario'])) {
    header('location: ../home');
    exit();
} else {
    $Email = $_SESSION['usuario']['login'];





    if (!file_exists('funcoes.php')) {
        include 'arquivos/funcoes.php';
    } else {
        include 'funcoes.php';
    }
    ?>


    <script type="text/javascript" >
        function show(toBlock) {
            setDisplay(toBlock, 'block');
        }
        function hide(toNone) {
            setDisplay(toNone, 'none');
        }
        function setDisplay(target, str) {
            document.getElementById(target).style.display = str;
        }




        function Mascara(o, f) {
            v_obj = o;
            v_fun = f;
            setTimeout("execmascara()", 1);
        }
        //Função que Executa os objetos
        function execmascara() {
            v_obj.value = v_fun(v_obj.value);
        }
        //Função que padroniza telefone (11) 4184-1241
        function Telefone(v) {
            v = v.replace(/\D/g, "").replace(/^(\d\d)(\d)/g, "($1) $2").replace(/(\d{4})(\d)/, "$1-$2");
            return v;
        }
        //Função que padroniza CPF
        function Cpf(v) {
            v = v.replace(/\D/g, "").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            return v;
        }

        $(document).ready(function () {

            // valida todos os campos ao deixar o foco deles
            $('#nome, #email, #tel, #cpf, #senha, #nascimento').blur(function () {
                validarCampo($(this));
            });

            /* VALIDAÇÃO ELEMENTOS */
            $('#user').submit(function (event) {

                var alerta = $('.alert');
                var alertaTexto = "";

                // valida o campo nome
                if ($('#nome').val() == "") {
                    alertaTexto += "Campo nome deve ser preenchido.<br>";
                }

                // valida o campo email
                if ($('#email').val() == "") {
                    alertaTexto += "Campo email deve ser preenchido.<br>";
                } else {
                    var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                    if (!regexEmail.test($('#email').val())) {
                        alertaTexto += "Campo email deve ser um email válido.<br>";
                    }
                }

                // valida o campo telefone
                if ($('#tel').val() == "") {
                    alertaTexto += "Campo telefone deve ser preenchido.<br>";
                }



                // valida o campo CPF
                if ($('#cpf').val() == "") {
                    alertaTexto += "Campo CPF deve ser preenchido.<br>";
                }

                // valida o campo nascimento
                if ($('#nascimento').val() == "") {
                    alertaTexto += "Campo nascimento deve ser preenchido.<br>";
                }

                // SE EXISTIR ERRO NA VALIDAÇÃO MOSTRA A MENSAGEM DE ERRO
                if (alertaTexto != "") {
                    alerta.html(alertaTexto);
                    alerta.show();
                    event.preventDefault(); // previne o formulário de ser submetido
                } else {
                    //alert('Formulário OK!'); // validação ok, todos os campos estão preenchidos
                }

            });
        });
    </script>

    </head>
    <body>
        <?php

        $db = conecta();



        $sql = "SELECT id, nome, email,senha,tel,cpf,nascimento,sexo,perf_img FROM cliente WHERE email = '$Email'";

        $rs = pg_query($sql);




        $row = pg_fetch_array($rs);

        echo "
        <div class='container jumbotron'>
        <h2 class='text-center'>Cadastro de Usuário</h2>
        <div class='alert alert-danger'></div>
        <hr>
        <form class='form-horizontal' action='../cadastre/att-usuario.php' method='POST' id='user'>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Nome</label>
                <div class='col-sm-9'>
                    <input name='nome' type='text' id='nome' maxlength='30' class='form-control' value='$row[1]' autofocus>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Email</label>
                <div class='col-sm-9'>
                    <input name='email' type='email' id='email' maxlength='30' class='form-control' value='$row[2]'>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Senha</label>
                <div class='col-sm-9'>"; ?>
                <div class='btn btn-info' id='alt_se'
                     onclick="show('senha'); hide('alt_se')">Alterar Senha</div>
        <?php
        echo "
                <input name='senha' type='password' id='senha' maxlength='30' class='form-control' placeholder='Alterar senha' style='display:none;'>
                    <!--<input name='senha' type='password' id='senha' maxlength='30' class='form-control' placeholder='Alterar senha'value='$row[3]>
                    <input name='senha' type='hidden' id='senha' maxlength='30' class='form-control' value='$row[3]'> -->
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Telefone</label>
                <div class='col-sm-9'>
                    <input name='tel' type='text' id='tel' maxlength='18' onKeyDown='Mascara(this, Telefone);
        ' onKeyPress='Mascara(this, Telefone);
        ' onKeyUp='Mascara(this, Telefone);'  class='form-control' value='$row[4]'>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>CPF</label>
                <div class='col-sm-9'>
                    <input maxlength='14' onKeyDown='Mascara(this, Cpf);
        ' onKeyPress='Mascara(this, Cpf);
        ' onKeyUp='Mascara(this, Cpf);' name='cpf' type='text' id='cpf' class='form-control' value='$row[5]'>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Nascimento</label>
                <div class='col-sm-9'>
                    <input  name='nascimento' type='date' id='nascimento' class='form-control' value='$row[6]'>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Sexo</label>
                <div class='col-sm-9'>
                    <div class='input-group-addon' style='background-color:transparent;'> ";
        if ($row[7] == '0') {
            $masc = "";
            $femin = "checked";
        } else {

            $masc = "checked";
            $femin = "";
        }
        echo
        "<input name='sexo' type='radio' id='0' value='0' $femin/> <label for='0' style='margin-right:20px'>Feminino</label>
                        <input name='sexo' type='radio' id='1' value='1' $masc/> <label for='1' >Masculino</label>
                    </div>
                </div>
            </div>
        <input type = 'hidden' name = 'mail_usu' value = '$Email'>
        <div class='nav'>
            <button type='button' class='btn btn-danger' onclick='javascript:history.back()'>Cancelar</button>
            <button type='submit' class='btn btn-success'>Enviar</button>
        </form>
    </div>
        ";

        pg_close($db);
        //}



        include 'estrutura/footer.php';
    }
        