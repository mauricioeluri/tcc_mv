<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        //if (!isset($_POST)) {
        //    echo 'nao tem post';
        //    //header('Location: http://www.google.com/');
        // }
        if (!isset($_REQUEST['id_func'])) {
            header('Location: ../funcionario/');
        }
//        if (!isset($_SESSION['admin'])) {
        //          header('location: ../home');
        //        exit();
        //  } else {
        $id_func = $_REQUEST['id_func'];
        include '../arquivos/estrutura/header.php';

        if (!file_exists('../funcoes.php')) {
            include '../arquivos/funcoes.php';
        } else {
            include '../funcoes.php';
        }
        ?>
        <script>
            function show(toBlock) {
                setDisplay(toBlock, 'block');
            }
            function hide(toNone) {
                setDisplay(toNone, 'none');
            }
            function setDisplay(target, str) {
                document.getElementById(target).style.display = str;
            }


            function Mascara(o, f) {
                v_obj = o;
                v_fun = f;
                setTimeout("execmascara()", 1);
            }
            //Função que Executa os objetos
            function execmascara() {
                v_obj.value = v_fun(v_obj.value);
            }
            //Função que padroniza telefone (11) 4184-1241
            function Telefone(v) {
                v = v.replace(/\D/g, "").replace(/^(\d\d)(\d)/g, "($1) $2").replace(/(\d{4})(\d)/, "$1-$2");
                return v;
            }
            //Função que padroniza CPF
            function Cpf(v) {
                v = v.replace(/\D/g, "").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d{1,2})$/, "$1-$2");
                return v;
            }

            //Função que padroniza CEP
            function Cep(v) {
                v = v.replace(/\D/g, "").replace(/^(\d{5})(\d)/, "$1-$2");
                return v;
            }


            $(document).ready(function () {


                //VALIDAÇÃO ELEMENTOS
                $('#cadastra').submit(function (event) {

                    var alerta = $('.alert');
                    var alertaTexto = "";
                    // valida o campo nome
                    if ($('#nome').val() == "") {
                        alertaTexto += "Campo nome deve ser preenchido.<br>";
                    }

                    /* valida o campo email
                     if ($('#email').val() == "") {
                     alertaTexto += "Campo email deve ser preenchido.<br>";
                     } else {
                     var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+);
                     if (!regexEmail.test($('#email').val())) {
                     alertaTexto += "Campo email deve ser um email válido.<br>";
                     } */


                    // valida o campo telefone
                    if ($('#tel').val() == "") {
                        alertaTexto += "Campo telefone deve ser preenchido.<br>";
                    }
                    // valida o campo CPF
                    if ($('#cpf').val() == "") {
                        alertaTexto += "Campo CPF deve ser preenchido.<br>";
                    }

                    // valida o campo cep
                    if ($('#rua').val() == "") {
                        alertaTexto += "Campo cep deve ser preenchido.<br>";
                    }


 //if (    !is_front_page) && (!is_single()) && (!is_page)

                    // valida o campo senha
                    if (( $('#senha1').val() > 0)   &&   ($('#senha1').val() < 7 )) {
                        alertaTexto += "Campo senha deve ser preenchido.<br>";
                    }

                    // valida o campo nascimento
                    if ($('#nascimento').val() == "") {
                        alertaTexto += "Campo nascimento deve ser preenchido.<br>";
                    }
                    // SE EXISTIR ERRO NA VALIDAÇÃO MOSTRA A MENSAGEM DE ERRO
                    if (alertaTexto !== "") {
                        alerta.html(alertaTexto);
                        alerta.show();
                        event.preventDefault(); // previne o formulário de ser submetido
                    } else {
                        alert('Cadastro realizado com sucesso!'); // validação ok, todos os campos estão preenchidos
                    }

                });
//SCRIPT DO CEP      //SCRIPT DO CEP      //SCRIPT DO CEP
                var $formGroup = $("#cep").closest('.col-sm-9');
                var errocep = 0;
                function limpa_formulário_cep() {
                    // Limpa valores do formulário de cep.
                    $("#rua").val("");
                    $("#bairro").val("");
                    $("#cidade").val("");
                    $("#uf").val("");
                }
                //Quando o valor do cep chega a 9
                $("#cep").keyup(function () {
                    var len = this.value.length;
                    if (len === 9) {
                        //Nova variável "cep" somente com dígitos.
                        var cep = $(this).val().replace(/\D/g, '');
                        //Verifica se campo cep possui valor informado.
                        if (cep !== "") {
                            //Expressão regular para validar o CEP.
                            var validacep = /^[0-9]{8}$/;
                            //Valida o formato do CEP.
                            if (validacep.test(cep)) {
                                //Preenche os campos com "..." enquanto consulta webservice.
                                $("#rua").val("...");
                                $("#bairro").val("...");
                                $("#cidade").val("...");
                                $("#uf").val("...");
                                //Consulta o webservice viacep.com.br
                                $.getJSON("http://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
                                    if (!("erro" in dados)) {
                                        //Atualiza os campos com os valores da consulta.
                                        $("#rua").val(dados.logradouro);
                                        $("#bairro").val(dados.bairro);
                                        $("#cidade").val(dados.localidade);
                                        $("#uf").val(dados.uf);
                                    } //end if.
                                    else {
                                        //CEP pesquisado não foi encontrado.
                                        limpa_formulário_cep();
                                        if (errocep == 0) {
                                            $formGroup.addClass('has-error');
                                            $formGroup.append('<div class="alert alert-danger" role="alert">Cep inválido.</div>');
                                            errocep++;
                                        }
                                    }
                                });
                            } //end if.
                        } //end if.


                    } else {
                        //toda teclada menor que 9
                        errocep = 0;
                        limpa_formulário_cep();
                    }
                });
            });
        </script>

    </head>
    <body>
        <?php
        $db = conecta();



        $sql = "Select * FROM funcionario WHERE id = '$id_func'";

        $rs = pg_query($sql);




        $row = pg_fetch_array($rs);

        echo $row[0];
        echo $row[1];
        echo $row[2];
        echo $row[3];
        echo $row[4];
        echo $row[5];
        echo $row[6];
        echo $row[7];
        echo $row[8];
        echo $row[9];
        echo $row[10];
        echo $row[11];
        echo $row[12];



        echo "
        <div class='container jumbotron'>
            <h2 class='text-center'>Cadastro de Funcionario</h2>
            <div class='alert alert-danger'></div>
            <hr>
            <form class='form-horizontal' action='att-funcionario.php' method='POST' id='cadastra'>
                <div class='form-group'>
                    <label class='col-sm-3 control-label'>Nome</label>
                    <div class='col-sm-9'>
                        <input
                            id='nome' name='nome' type='text' maxlength='30' class='form-control' value='$row[1]' autofocus>
                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-sm-3 control-label'>Email</label>
                    <div class='col-sm-9'>
                        <input name='email' type='email' id='email' maxlength='30' class='form-control' value='$row[2]'>
                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-sm-3 control-label'>Telefone</label>
                    <div class='col-sm-9'>
                        <input name='tel' type='text' id='tel' maxlength='18' onKeyDown='Mascara(this, Telefone);
                               ' onKeyPress='Mascara(this, Telefone);
                               ' onKeyUp='Mascara(this, Telefone);
                               '  class='form-control' value='$row[3]'>
                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-sm-3 control-label'>CPF</label>
                    <div class='col-sm-9'>
                        <input maxlength='14' onKeyDown='Mascara(this, Cpf);
                               ' onKeyPress='Mascara(this, Cpf);
                               ' onKeyUp='Mascara(this, Cpf)
                        ;
                               ' name='cpf' type='text' id='cpf' class='form-control' value='$row[4]'>
                    </div>
                </div>

                <div class='form-group'>
                    <label class='col-sm-3 control-label'>Cep</label>
                    <div class='col-sm-9'>
                        <input maxlength='9' onKeyDown='Mascara(this, Cep);
                               ' onKeyPress='Mascara(this, Cep);
                               ' onKeyUp='Mascara(this, Cep);
                               '
                               name='cep' type='text' id='cep' class='form-control' value='$row[5]'>
                    </div>
                </div>
                <div class='form-group'>";
        ?>
        <label class='col-sm-3 control-label'>Senha</label>
        <div class='col-sm-9'>
            <!--<input name='senha' type='password' id='senha' maxlength='30' class='form-control' value='$row[6]'>-->
            <div class='btn btn-info' id='alt_se'
                 onclick="show('senha1');
                     hide('alt_se')">Alterar Senha</div>




            <?php
            echo"
                <input name='senha' type='password' id='senha1' maxlength='30' class='form-control' placeholder='Alterar senha' style='display:none;'>
                    <!--<input name='senha' type='password' id='senha' maxlength='30' class='form-control' placeholder='Alterar senha'value='$row[3]>
                    <input name='senha' type='hidden' id='senha' maxlength='30' class='form-control' value='$row[3]'> -->
            </div>
            </div>



                <div class='form-group'>
                    <label class='col-sm-3 control-label'>Nascimento</label>
                    <div class='col-sm-9'>
                        <input  name='nascimento' type='date' id='nascimento' class='form-control' value='$row[7]'>
                    </div>
                </div>
                <div class='form-group'>
                    <label class='col-sm-3 control-label'>Sexo</label>
                    <div class='col-sm-9'>
                        <div class='input-group-addon' style='background-color:transparent;'> ";
            if ($row[8] == 'f') {
                $masc = "";
                $femin = "checked";
            } else {
                $masc = "checked";
                $femin = "";
            }
            echo
            "<input name='sexo' class='sexo' type='radio' id='f' value='f' $femin> <label for='f' style='margin-right:20px'>Feminino</label>
                            <input name='sexo' class='sexo' type='radio' id='m' value='m' $masc/> <label for='m' >Masculino</label>
                        </div>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='inputPassword' class='col-sm-3 control-label'>Rua</label>
                    <div class='col-sm-9'>
                        <input  name='rua' type='text' id='rua' class='form-control' size='60' readonly placeholder='Campo preenchido automaticamente...' value='$row[9]'>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='inputPassword' class='col-sm-3 control-label'>Bairro</label>
                    <div class='col-sm-9'>
                        <input name='bairro' type='text' id='bairro' class='form-control' size='60' readonly placeholder='Campo preenchido automaticamente...' value='$row[10]'>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='inputPassword' class='col-sm-3 control-label'>Cidade</label>
                    <div class='col-sm-9'>
                        <input name='cidade' type='text' id='cidade' class='form-control' size='40' readonly placeholder='Campo preenchido automaticamente...' value='$row[11]'>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='inputPassword' class='col-sm-3 control-label'>Estado</label>
                    <div class='col-sm-9'>
                        <input name='uf' type='text' id='uf' size='2' class='form-control' readonly placeholder='Campo preenchido automaticamente...' value='$row[12]'>
                    </div>
                </div>
                <input type = 'hidden' name = 'id_func' value = '$id_func'>
                <div class='nav'>                
                    <button type='submit' class='btn btn-danger' onclick='javascript:history.back()'>Cancelar</button>
                    <button type='submit' class='btn btn-success'>Enviar</button>
                </div>
            </form>
        </div>
        ";

            pg_close($db);
            //}



            include '../arquivos/estrutura/footer.php';

            