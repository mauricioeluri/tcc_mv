<?php
session_start();
if (!isset($_SESSION['admin'])) {
    header('location: ../index.php');
    exit();
}
?>

<link rel=icon type=image/png href=../../arquivos/img/logo2.png>
<meta charset="UTF-8">
<meta name=description content="O ElUri Framework pode agilizar muito o seu desenvolvimento web. Contando com um simples modelo de website pronto, e com várias ferramentas e funções já configuradas.">
<meta name=viewport content="width=device-width, initial-scale=1">
<link href="../../arquivos/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../../arquivos/css/normal/estrutura.css" rel="stylesheet" type="text/css"/>
<script src="../../arquivos/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<!--[if lt IE 9]>
    <script>
alert('Você está usando um navegador antigo!\n\nAlguns itens podem não aparecer corretamente.\nConsidere atualizar para uma versão mais recente.');
</script>
<![endif]-->
</head>
<body style="background-image: url('../../arquivos/img/fundoAdmin.png');">
    <nav class="navbar navbar-default navbar-static-top">
        <div id='topoTudo'>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="log" href="../home">
                   
                    <p class="navbar-brand">Area administrativa</p></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <div id='topoItem'>
                    <ul class="nav navbar-nav">
                        <li><a href="../funcionario" class="link">Funcionário</a></li>
                        <li><a href="../animais" class="link">Animais</a></li>
                        <li><a href="../usuario" class="link">Usuário</a></li>
                        <li><a href="../adocoes" class="link">Adoções</a></li>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-user"></i>
                                    <strong>
                                        <?php
                                        echo $_SESSION['admin']['usuario'];
                                        ?>
                                    </strong>
                                    <i class="fa fa-sort-desc"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="navbar-login">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <p class="text-center">
                                                        <i class="fa fa-user-secret fa-4x"></i>
                                                    </p>
                                                </div>
                                                <div class="col-lg-8">
                                                    <p>
                                                        <a href="../arquivos/alt_dados.php" class="btn btn-primary btn-block btn-sm semborda">Atualizar dados</a>
                                                        <a href="../logout.php" class="btn btn-danger btn-block semborda">Sair</a>
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </ul>

                </div>
            </div>
        </div>
    </nav>
    <div id='content'>
