<h1>Adoções</h1>





<script>
    function apagar() {
        decisao = confirm("Você tem certeza que deseja apagar esta adoção do sistema?");
        if (decisao) {
            alert("Adoção apagada com sucesso!");
        } else {
            event.preventDefault();
        }
    }
</script>


<?php
include "../funcoes.php";
$db = conecta();


$rs = pg_query("select adocao.id,
       to_char(data_suposta, 'DD/MM/YYYY'),
	c.id,
	c.nome,
	c.tel,
	c.email,
	a.id,
	a.nome,
	to_char(a.previsao_adocao, 'DD/MM/YYYY'),
	a.descricao,
	to_char(data_confirm, 'DD/MM/YYYY')
	from adocao 
 inner join cliente c on (adocao.id_clie = c.id) 
 inner join animal a on (adocao.id_anim = a.id)
   group by adocao.id, 
	    data_suposta,
	    c.id,
	    c.nome,
	    c.tel,
	    c.email,
	    a.id,
	    a.nome,
	    a.previsao_adocao,
	    a.descricao,
	    data_confirm
	    order by adocao.id desc;");
?>
<table border="1" cellpadding = 13;>
    <caption class="tp">Dados Cadastrados</caption>
    <thead  bgcolor = "#0099FF">
    <th>Data Suposta</th>
    <th>Nome Cliente</th>
    <th>Telefone Cliente</th>
    <th>Email Cliente</th>
    <th>Nome Animal</th>
    <th>Previsão Adoção</th>
    <th>Descrição</th>
    <th>Data Confirmada</th>


</thead>

<?php
$z = 0;
$cor1 = '#fff';
$cor2 = '#ddd';
while ($row = pg_fetch_array($rs)) {
    echo "<tr bgcolor='";

    if ($z == 0) {
        echo $cor1;
        $z++;
    } else {
        echo $cor2;
        $z = 0;
    }
    ?> '>    
    <form action="confirma-adocao.php" method="POST">
        <input type="hidden" name="id" value="<?= $row[0] ?>"/>
        <input type="hidden" name="id_clie" value="<?= $row[2] ?>"/>
        <input type="hidden" name="id_anim" value="<?= $row[6] ?>"/>
        <input type="hidden" name="data_supo" value="<?= $row[1] ?>"/>
        <td><?= $row[1] ?></td>
        
        <td><?= $row[3] ?></td>
        <td><?= $row[4] ?></td>
        <td><?= $row[5] ?></td>
        <td><?= $row[7] ?></td>
        <td><?= $row[8] ?></td>
        <td><?= $row[9] ?></td>
        <td><?php
            if ($row[10] == null) {
                echo "<button type='submit' class='btn btn-success center-block'>Confirmar!</button>";
            } else {
                echo $row[10];
            }
            ?></td>
    </form>

    </tr>
    <?php
}

pg_close($db);
?> 
</table>