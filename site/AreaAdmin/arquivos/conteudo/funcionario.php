<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        background: red;
        cursor: inherit;
        display: block;
    }
    input[readonly] {
        background-color: white !important;
        cursor: text !important;
    }
</style>
<script>
    
    $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
    
<?php
$erro = $_SESSION['admin']['erro'];
if ($erro !== '') {
    echo "alert('$erro');";
    $_SESSION['admin']['erro'] = '';
}
?>
    function show(toBlock) {
        setDisplay(toBlock, 'block');
    }
    function hide(toNone) {
        setDisplay(toNone, 'none');
    }
    function setDisplay(target, str) {
        document.getElementById(target).style.display = str;
    }

    function apagar() {
        decisao = confirm("Você tem certeza que deseja apagar este funcionário do sistema?");
        if (decisao) {
            alert("Funcionário apagado com sucesso!");
        } else {
            event.preventDefault();
        }
    }

</script>
<h1>Funcionário</h1>
<div id="clickMeId">
    <a class="btn btn-primary btn-large adicionar" onclick="show('formulario');
            hide('clickMeId')">
        Adicionar
    </a>
</div>
<div id="formulario" style="display:none;">

    <div class="container jumbotron">
        <div style="float: right;" id="clickMeId2" onclick="show('clickMeId');
                hide('formulario')">
            <i style="color:#981C19;" class="fa fa-times fa-2x"></i>
        </div>
        <h2 class="text-center">Cadastro de Funcionario</h2>
        <div class="alert alert-danger"></div>
        <hr>
        <form class="form-horizontal" action="grava-funcionario.php" method="POST" id="cadastra" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Foto de usuário</label>
                <!-- <div class="col-sm-9"> -->
                    <!-- <input type="file" name="fileToUpload" > -->

                <div class="col-sm-9">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-primary btn-file">
                                Browse&hellip; <input name="fileToUpload" type="file" accept="image/*" >
                            </span>
                        </span>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
                <!-- </div> -->
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nome</label>
                <div class="col-sm-9">
                    <input
                        id="nome" name="nome" type="text" maxlength="30" class="form-control" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input name="email" type="email" id="email" maxlength="30" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Telefone</label>
                <div class="col-sm-9">
                    <input name="tel" type="text" id="tel" maxlength="18" onKeyDown="Mascara(this, Telefone);" onKeyPress="Mascara(this, Telefone);" onKeyUp="Mascara(this, Telefone);"  class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">CPF</label>
                <div class="col-sm-9">
                    <input maxlength="14" onKeyDown="Mascara(this, Cpf);" onKeyPress="Mascara(this, Cpf);" onKeyUp="Mascara(this, Cpf)
                                    ;" name="cpf" type="text" id="cpf" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Cep</label>
                <div class="col-sm-9">
                    <input maxlength="9" onKeyDown="Mascara(this, Cep);" onKeyPress="Mascara(this, Cep);" onKeyUp="Mascara(this, Cep);"
                           name="cep" type="text" id="cep" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Senha</label>
                <div class="col-sm-9">
                    <input name="senha" type="password" id="senha" maxlength="30" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nascimento</label>
                <div class="col-sm-9">
                    <input  name="nascimento" type="date" id="nascimento" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Sexo</label>
                <div class="col-sm-9">
                    <div class="input-group-addon" style="background-color:transparent;">
                        <input name="sexo" class="sexo" type="radio" id="f" value="f" checked="checked"/> <label for="f" style="margin-right:20px">Feminino</label>
                        <input name="sexo" class="sexo" type="radio" id="m" value="m"/> <label for="m" >Masculino</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">Rua</label>
                <div class="col-sm-9">
                    <input  name="rua" type="text" id="rua" class="form-control" size="60" readonly placeholder="Campo preenchido automaticamente...">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">Bairro</label>
                <div class="col-sm-9">
                    <input name="bairro" type="text" id="bairro" class="form-control" size="60" readonly placeholder="Campo preenchido automaticamente...">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">Cidade</label>
                <div class="col-sm-9">
                    <input name="cidade" type="text" id="cidade" class="form-control" size="40" readonly placeholder="Campo preenchido automaticamente...">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">Estado</label>
                <div class="col-sm-9">
                    <input name="uf" type="text" id="uf" size="2" class="form-control" readonly placeholder="Campo preenchido automaticamente...">
                </div>
            </div>
            <div class='nav'>
                <button class='btn btn-danger' onclick="show('clickMeId');
                        hide('formulario')" type="button">Cancelar</button>
                <button type="submit" class="btn btn-success">Enviar</button>
            </div>
        </form>
    </div>




</div>

<script>

    function Mascara(o, f) {
        v_obj = o;
        v_fun = f;
        setTimeout("execmascara()", 1);
    }
    //Função que Executa os objetos
    function execmascara() {
        v_obj.value = v_fun(v_obj.value);
    }
    //Função que padroniza telefone (11) 4184-1241
    function Telefone(v) {
        v = v.replace(/\D/g, "").replace(/^(\d\d)(\d)/g, "($1) $2").replace(/(\d{4})(\d)/, "$1-$2");
        return v;
    }
    //Função que padroniza CPF
    function Cpf(v) {
        v = v.replace(/\D/g, "").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d{1,2})$/, "$1-$2");
        return v;
    }

    //Função que padroniza CEP
    function Cep(v) {
        v = v.replace(/\D/g, "").replace(/^(\d{5})(\d)/, "$1-$2");
        return v;
    }


    $(document).ready(function () {


        //VALIDAÇÃO ELEMENTOS
        $('#cadastra').submit(function (event) {

            var alerta = $('.alert');
            var alertaTexto = "";
            // valida o campo nome
            if ($('#nome').val() == "") {
                alertaTexto += "Campo nome deve ser preenchido.<br>";
            }

            /* valida o campo email
             if ($('#email').val() == "") {
             alertaTexto += "Campo email deve ser preenchido.<br>";
             } else {
             var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+);
             if (!regexEmail.test($('#email').val())) {
             alertaTexto += "Campo email deve ser um email válido.<br>";
             } */


            // valida o campo telefone
            if ($('#tel').val() == "") {
                alertaTexto += "Campo telefone deve ser preenchido.<br>";
            }
            // valida o campo CPF
            if ($('#cpf').val() == "") {
                alertaTexto += "Campo CPF deve ser preenchido.<br>";
            }

            // valida o campo cep
            if ($('#rua').val() == "") {
                alertaTexto += "Campo CEP deve ser preenchido.<br>";
            }
            //valida senha
            if ($('#senha').val().length <= 6) {
                alertaTexto += "Senha tem que possuir obrigatoriamente mais de 6 digitos.<br>";
            }
            // valida o campo nascimento
            if ($('#nascimento').val() == "") {
                alertaTexto += "Campo nascimento deve ser preenchido.<br>";
            }
            // SE EXISTIR ERRO NA VALIDAÇÃO MOSTRA A MENSAGEM DE ERRO
            if (alertaTexto != "") {
                alerta.html(alertaTexto);
                alerta.show();
                event.preventDefault(); // previne o formulário de ser submetido
            } else {
                alert('Cadastro realizado com sucesso!'); // validação ok, todos os campos estão preenchidos
            }

        });
        //SCRIPT DO CEP      //SCRIPT DO CEP      //SCRIPT DO CEP
//        var $formGroup = $("#cep").closest('.col-sm-9');
//        var errocep = 0;
        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep.
            $("#rua").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#uf").val("");
        }
        //Quando o valor do cep chega a 9
        $("#cep").on('keyup keypress blur', function (e) {
            var len = this.value.length;
            if (len === 9) {
                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');
                //Verifica se campo cep possui valor informado.
                if (cep !== "") {
                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;
                    //Valida o formato do CEP.
                    if (validacep.test(cep)) {
                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#uf").val("...");
                        //Consulta o webservice viacep.com.br
                        $.getJSON("http://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
//                                if (errocep == 0) {
//                                    $formGroup.addClass('has-error');
//                                    $formGroup.append('<div class="alert alert-danger" role="alert">Cep inválido.</div>');
//                                    errocep++;
//                                }
                            }
                        });
                    } //end if.
                } //end if.


            } else {
                //toda teclada menor que 9
//                errocep = 0;
                limpa_formulário_cep();
            }
        });
    });
</script>


<?php
include "../funcoes.php";

$db = conecta();


$rs = pg_query("SELECT id, nome, email,tel,cpf,cep,senha,nascimento,sexo,rua,bairro,cidade,uf,perf_img FROM funcionario order by id desc");
?>


<?php
while ($row = pg_fetch_array($rs)) {



    echo "<div class='panel panel-default jumbotron caixa'>
    <div class='panel-body'>
        <div class='col-sm-12'>
        <form action='modifica-funcionario.php' method='POST'> 
                <input type='hidden' name='id_func' value='$row[0]'>
                    <button type='submit' class='btn btn-info editar'>
                    <i class='fa fa-pencil-square-o'></i>
                        Editar
                    </button>
            </form>
            
            <form action='apaga-funcionario.php' method='POST'>
                <input type='hidden' name='id_func' value='$row[0]'>
                <button type='submit' class='btn btn-danger excluir' onclick='apagar();'>
                    <i class='fa fa-times-circle-o fa-1x'></i>
                    Excluir
                </button>
            </form>";
    echo "
            <img src='";
    if ($row[13] == '') {
        $perf = '../../arquivos/img/padrao.png';
    } else {
        $perf = '../../arquivos/img/funcionario/' . $row[13];
    }
    echo"$perf' class='col-sm-4 img-user'>

            <div class='col-sm-8'><h3 class='nome_user'>$row[1]</h3></div>
            <p class='dados_user2 dados_user1'>Email: $row[2]</p>
            <p class='dados_user2 dados_user1'>Telefone: $row[3]</p>
            <p class='dados_user2 dados_user1'>Idade: ";
    $dob = $row[7];
    echo ageCalculator($dob);
    echo " anos</p>
        </div>
        <div class='clearfix'></div>
        <hr>
        <div class='div_50' style='float:left'><p class='dados_user1'>Cpf: $row[4]</p></div>
        <div class='div_50' style='float:left'><p class='dados_user1'>Bairro: $row[10]</p></div>
        <div class='div_50' style='float:left'><p class='dados_user1'>Cidade: Bagé</p></div>
        <div class='div_50' style='float:left'><p class='dados_user1'>$row[9]</p></div>
        <div class='div_dados_user2 div_50' style='float:left;'><p class='dados_user1'>Sexo: ";
    if ($row[8] == 'm') {
        $sexo = 'Masculino';
    } else {
        $sexo = 'Feminino';
    }

    //$dob = birthdate in the form "YYYY-MM-DD"
//Extract the month, day and year from the $dob
//Compare to current month, day, year to get AGE



    echo " $sexo</p></div>
        <div class='div_50' style='float:right;'><p class='dados_user1'>Uf: $row[10]</p></div>
    </div>
</div>";
}

pg_close($db);
?> 



<!--
    <table border="1" cellpadding = 13;>
        <caption class="tp">Dados Cadastrados</caption>

        <td>
            <form action="modifica-funcionario.php" method="POST"> 
                <input type="hidden" name="id_func" value="<?= $row[0] ?>">
                <button class="btn-default btn_esq" type="submit">
                    <i class="fa fa-pencil-square-o editar"></i>
                </button>
            </form>
            <form action="apaga-funcionario.php" method="POST">
                <input type="hidden" name="id_func" value="<?= $row[0] ?>">
                <button class="btn-default" type="submit" onclick="apagar();">
                    <i class="fa fa-times-circle-o fa-1x excluir"></i>
                </button>
            </form>
        </td>
        <td><?= $row[1] ?></td>
        <td><?= $row[2] ?></td>
        <td><?= $row[3] ?></td>
        <td><?= $row[4] ?></td>
        <td><?= $row[5] ?></td>
        <td><?= $row[6] ?></td>
        <td><?= $row[7] ?></td>
        <td><?php
if ($row[8] == '0') {
    $sexo = "Feminino";
} else {
    $sexo = "Masculino";
}
echo $sexo;
?></td>
        <td><?= $row[9] ?></td>
        <td><?= $row[10] ?></td>
        <td><?= $row[11] ?></td>
        <td><?= $row[12] ?></td>

    </tr>   

</table>-->